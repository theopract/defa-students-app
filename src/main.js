import Vue from 'vue'
import App from './App.vue'

export const eventBus = new Vue({
  data: {
    student: {
      name: '',
      surname: '',
      birthDate: '',
      group: ''
    }
  }
});

const vm = new Vue({
  el: '#app',
  render: h => h(App)
})
