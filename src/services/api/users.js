import axios from 'axios'

export default {
  getUser () {
    return axios.get('https://randomuser.me/api/?nat=us,gb')
      .then(response => {
        return response.data.results[0].name;
      })
  }
}