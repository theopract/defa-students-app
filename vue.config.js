// файл vue.config.js расположен в корне вашего репозитория
// убедитесь, что обновили `yourProjectName` на имя вашего проекта GitLab

module.exports = {
  baseUrl: process.env.NODE_ENV === 'production'
    ? '/defa-students-app/'
    : '/'
}